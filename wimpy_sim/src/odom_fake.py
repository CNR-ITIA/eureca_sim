#!/usr/bin/env python
import math
from math import sin, cos, pi

import rospy
import tf
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3


posx = 0.0
posy = 0.0
posw = 0.0

velx =  0.0
vely =  0.0
velw = 0.0

def callback(msg):
    global velx
    global vely
    global velw
    velx = msg.linear.x
    vely = msg.linear.y
    velw = msg.angular.z

rospy.init_node('odometry_fake_wimpy')

odom_pub = rospy.Publisher("odom", Odometry, queue_size=50)
odom_broadcaster = tf.TransformBroadcaster()
rospy.Subscriber("cmd_vel", Twist, callback)

current_time = rospy.Time.now()
last_time = rospy.Time.now()

r = rospy.Rate(25.0)
while not rospy.is_shutdown():
    current_time = rospy.Time.now()
    dt = (current_time - last_time).to_sec()
   
    posx += (velx*cos(posw) - vely*sin(posw))*dt
    posy += (velx*sin(posw) + vely*cos(posw))*dt
    posw += velw*dt

    odom_quat = tf.transformations.quaternion_from_euler(0, 0, posw)

    odom_broadcaster.sendTransform(
        (posx, posy, 0.),
        odom_quat,
        current_time,
        "wimpy/base_footprint",
        "map"
    )

    odom = Odometry()
    odom.header.stamp = current_time
    odom.header.frame_id = "wimpy/odom"

    odom.pose.pose = Pose(Point(posx, posy, 0.), Quaternion(*odom_quat))

    odom.child_frame_id = "wimpy/base_link"
    odom.twist.twist = Twist(Vector3(velx, vely, 0), Vector3(0, 0, velw))

    odom_pub.publish(odom)

    last_time = current_time
    r.sleep()
