#!/usr/bin/env python
<<<<<<< HEAD
import math
from math import sin, cos, pi

=======

import math
from math import sin, cos, pi
# import keyboard
>>>>>>> 9e7f2621356f60210352c9886ff047c033470a65
import rospy
import tf
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3
<<<<<<< HEAD

=======
import threading
import os
import time
from map_server.srv import LoadMap
import rospkg
from dispatcher.msg import status_rob
>>>>>>> 9e7f2621356f60210352c9886ff047c033470a65

posx = 0.0
posy = 0.0
posw = 0.0

<<<<<<< HEAD
=======
posxo = 0.0
posyo = 0.0
poswo = 0.0

posxp = 0.0
posyp = 0.0
poswp = math.pi/2.0

>>>>>>> 9e7f2621356f60210352c9886ff047c033470a65
velx =  0.0
vely =  0.0
velw = 0.0

<<<<<<< HEAD
=======
dock_popeye = 0.0
dock_olivia = 0.0

popeye_state  = status_rob()
popeye_state.floor = 2
olivia_state  = status_rob()
olivia_state.floor = 2
sweepee_state = status_rob()
sweepee_state.floor = 2
wimpy_state   = status_rob()
wimpy_state.floor = 2

rospack = rospkg.RosPack()
change_map = rospy.ServiceProxy('/change_map', LoadMap)
path_maps = rospack.get_path('sweepee_sim')
path_maps = path_maps + "/maps/"

def user_in(invar):
    global dock_popeye
    global dock_olivia
    global olivia_state
    global popeye_state
    global sweepee_state
    global wimpy_state
    while True and not rospy.is_shutdown():
        os.system("clear")
        try:  
            nb = raw_input('Press a button then Enter to dock or release a cart: \n o - to dock iiwa cart \n p - to dock hat rack robot cart \n m0 : change map to groud floor \n m1 : change map to first floor \n m2 : change map to second floor \n \n ')
                            
            if nb == "o":
                dock_olivia = 1.0 if dock_olivia == 0.0 else 0.0
                olivia_state.docked = True if dock_olivia == 0.0 else False
            if nb == "p":
                dock_popeye = 1.0 if dock_popeye == 0.0 else 0.0
                popeye_state.docked = True if dock_popeye == 0.0 else False
            if nb == "m0":
                change_map(path_maps + "zero.yaml")
                popeye_state.floor = 0
                olivia_state.floor = 0
                sweepee_state.floor = 0
                wimpy_state.floor = 0
            if nb == "m1":
                change_map(path_maps + "first.yaml")
                popeye_state.floor = 1
                olivia_state.floor = 1
                sweepee_state.floor = 1
                wimpy_state.floor = 1
            if nb == "m2":
                change_map(path_maps + "second.yaml")
                popeye_state.floor = 2
                olivia_state.floor = 2
                sweepee_state.floor = 2
                wimpy_state.floor = 2
        except Exception as e:
            print(e)
            pass       
        time.sleep(0.2)


>>>>>>> 9e7f2621356f60210352c9886ff047c033470a65
def callback(msg):
    global velx
    global vely
    global velw
    velx = msg.linear.x
    vely = msg.linear.y
    velw = msg.angular.z

rospy.init_node('odometry_fake')

<<<<<<< HEAD
odom_pub = rospy.Publisher("odom", Odometry, queue_size=50)
=======
odom_pub       = rospy.Publisher("odom", Odometry, queue_size=50)
popeye_status  = rospy.Publisher("/popeye/status" , status_rob, queue_size=1)
olivia_status  = rospy.Publisher("/olivia/status" , status_rob, queue_size=1)
sweepee_status = rospy.Publisher("/sweepee/status", status_rob, queue_size=1)
wimpy_status   = rospy.Publisher("/wimpy/status"  , status_rob, queue_size=1)

>>>>>>> 9e7f2621356f60210352c9886ff047c033470a65
odom_broadcaster = tf.TransformBroadcaster()
rospy.Subscriber("/cmd_vel", Twist, callback)

current_time = rospy.Time.now()
last_time = rospy.Time.now()

<<<<<<< HEAD
r = rospy.Rate(25.0)
while not rospy.is_shutdown():
    current_time = rospy.Time.now()
    dt = (current_time - last_time).to_sec()
   
    posx += (velx*cos(posw) - vely*sin(posw))*dt
    posy += (velx*sin(posw) + vely*cos(posw))*dt
    posw += velw*dt

    odom_quat = tf.transformations.quaternion_from_euler(0, 0, posw)

    odom_broadcaster.sendTransform(
        (posx, posy, 0.),
        odom_quat,
        current_time,
        "base_footprint",
        "map"
    )

    odom = Odometry()
    odom.header.stamp = current_time
    odom.header.frame_id = "odom"

    odom.pose.pose = Pose(Point(posx, posy, 0.), Quaternion(*odom_quat))

    odom.child_frame_id = "base_link"
    odom.twist.twist = Twist(Vector3(velx, vely, 0), Vector3(0, 0, velw))

    odom_pub.publish(odom)

    last_time = current_time
    r.sleep()
=======
x = threading.Thread(target=user_in, args=(1,))
x.start()

r = rospy.Rate(25.0)

while not rospy.is_shutdown():
    try:
        current_time = rospy.Time.now()
        dt = (current_time - last_time).to_sec()
    
        posx += (velx*cos(posw) - vely*sin(posw))*dt
        posy += (velx*sin(posw) + vely*cos(posw))*dt
        posw += velw*dt

        posxo = posx if dock_olivia==1.0 else posxo
        posyo = posy if dock_olivia==1.0 else posyo
        poswo = posw if dock_olivia==1.0 else poswo

        posxp = posx if dock_popeye==1.0 else posxp
        posyp = posy if dock_popeye==1.0 else posyp
        poswp = (posw + math.pi/2.0) if dock_popeye==1.0 else poswp

        odom_quat_sweepee = tf.transformations.quaternion_from_euler(0, 0, posw)
        odom_quat_popeye  = tf.transformations.quaternion_from_euler(0, 0, poswp)
        odom_quat_olivia  = tf.transformations.quaternion_from_euler(0, 0, poswo)

        odom_broadcaster.sendTransform(
            (posx, posy, 0.),
            odom_quat_sweepee,
            current_time,
            "base_footprint",
            "map"
        )
        
        odom_broadcaster.sendTransform(
            (posxo, posyo, 0.),
            odom_quat_olivia,
            current_time,
            "iiwa_cart_base_link",
            "map"
        )        
        
        odom_broadcaster.sendTransform(
            (posxp, posyp, 0.),
            odom_quat_popeye,
            current_time,
            "popeye_cart",
            "map"
        )
        
        odom = Odometry()
        odom.header.stamp = current_time
        odom.header.frame_id = "odom"

        odom.pose.pose = Pose(Point(posx, posy, 0.), Quaternion(*odom_quat_sweepee))

        odom.child_frame_id = "base_link"
        odom.twist.twist = Twist(Vector3(velx, vely, 0), Vector3(0, 0, velw))

        odom_pub.publish(odom)
        popeye_state.header.stamp = rospy.Time.now()
        olivia_state.header.stamp = rospy.Time.now()
        sweepee_state.header.stamp = rospy.Time.now()
        wimpy_state.header.stamp = rospy.Time.now()
        popeye_status.publish(popeye_state)
        olivia_status.publish(olivia_state)
        sweepee_status.publish(sweepee_state)
        wimpy_status.publish(wimpy_state)

        last_time = current_time
        r.sleep()
    except Exception as e:
        print(e)
        pass
>>>>>>> 9e7f2621356f60210352c9886ff047c033470a65
