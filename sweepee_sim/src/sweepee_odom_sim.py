#!/usr/bin/env python
import rospy
import tf
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Pose,Point,Quaternion
import numpy as np
import quaternion
import math

rospy.init_node('sweepee_odom_simfal', anonymous=True,disable_signals=False)  
pub_odom        = rospy.Publisher('/sweepee/odom_simfal', Odometry, queue_size=1)  
pub_odom_olivia = rospy.Publisher('/olivia/odom_simfal', Odometry, queue_size=1)  
pub_odom_popeye = rospy.Publisher('/popeye/odom_simfal', Odometry, queue_size=1)  
pub_odom_bluto  = rospy.Publisher('/bluto/odom_simfal', Odometry, queue_size=1) 

 
listener = tf.TransformListener()
rate_sim = rospy.Rate(10) 

rotw   = []
transw = []
rot    = []
trans  = []

x_shift = [0,  5.95 , 11.6]
y_shift = [0, -0.4  , -2.75]

docked_obj = "none"
odom_sim = Odometry()
odom_sim.header.frame_id = "map_sweepee"

while True and not rospy.is_shutdown():
    try:
        (trans,rot) = listener.lookupTransform('/map_sweepee','/sweepee/base_footprint', rospy.Time(0))
    except:
        continue
    
    floor = rospy.get_param("/sweepee/status/floor")
        
    odom_sim.header.stamp    =  rospy.Time.now()
    
    if floor == 0:
        odom_sim.pose.pose       =  Pose(Point(trans[0],trans[1],0),Quaternion(rot[0],rot[1],rot[2],rot[3]))    
    elif floor == 1:
        baseq = np.quaternion(rot[0],rot[1],rot[2],rot[3])
        rot_q = np.quaternion(0.707,0.707,0,0)
        pd    = baseq * rot_q
        odom_sim.pose.pose       =  Pose(Point(trans[1] + x_shift[floor] ,-trans[0]+ y_shift[floor] , 1.7),Quaternion(pd.components[0],pd.components[1],pd.components[2],pd.components[3]))    
    elif floor == 2:
        baseq = np.quaternion(rot[0],rot[1],rot[2],rot[3])
        rot_q = np.quaternion(0.707,0.707,0,0)
        pd    = baseq * rot_q
        odom_sim.pose.pose       =  Pose(Point(trans[1] + x_shift[floor] ,-trans[0]+ y_shift[floor] , 3.1),Quaternion(pd.components[0],pd.components[1],pd.components[2],pd.components[3])) 
        
    pub_odom.publish(odom_sim)
    
    if rospy.has_param("/sweepee/status/docked_obj"):
        docked_obj = rospy.get_param("/sweepee/status/docked_obj")
    
    if docked_obj == "popeye":
        pub_odom_popeye.publish(odom_sim)
    elif docked_obj == "olivia":
        pub_odom_olivia.publish(odom_sim)
    elif docked_obj == "bluto":
        pub_odom_bluto.publish(odom_sim)
    
    rate_sim.sleep()
    
